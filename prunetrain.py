"""
 Contains implementation of "PruneTrain: Fast Neural Network Training by Dynamic Sparse Model Reconfiguration"
 https://arxiv.org/abs/1901.09290
"""

import math
from torch_backend import *

from collections import OrderedDict


def model_summary(model, x=torch.randn(1, 3, 32, 32).cuda()):
    """Print model summary for input with layers types and dimensions"""
    for n, m in list(model.named_modules())[1:]:
        try:
            x = m(x)
            print(f"{n}\t{type(m).__name__}\t{x.size()}")
        except Exception as e:
            print(f"{n}\t{type(m)}\t{e}")


def print_back_graph(grad_fn):
    """Print autograd backward graph with tensor and gradient sizes
    Adapted from https://stackoverflow.com/a/53024678
    """
    print(grad_fn)
    for n in grad_fn.next_functions:
        if n[0]:
            if hasattr(n[0], "variable"):
                try:
                    tensor = n[0].variable
                    print(f"{n[0]} tensor: {list(tensor.size())} grad: {list(tensor.grad.size())}")
                except AttributeError as e:
                    print(e)
                    print_back_graph(n[0])
            else:
                print_back_graph(n[0])


def mark_layers(net):
    """Mark first conv layer and last fc layer"""
    first_conv = None
    last_fc = None
    for m in net.children():
        if type(m) == nn.Conv2d and first_conv is None:
            first_conv = m
            m.first = True
        elif type(m) == nn.Linear:
            last_fc = m
    if last_fc is not None:
        last_fc.last = True


def compute_group_lasso_penalty(net):
    """Compute group lasso penalty.

    From PruneTrain paper:
        We do not apply group lasso to the input channels
        of the first convolution layer and the output neurons of the
        last fully-connected layer, because the input data and output
        predictions of a CNN have logical significance and should
        always be dense.
    """

    lasso_ch = []

    def group_lasso(m):
        if isinstance(m, nn.Conv2d):
            w = m.weight

            # not first conv layer
            if not hasattr(m, 'first'):
                lasso_ch.append(w.norm(p=2, dim=[0, 2, 3]))

            lasso_ch.append(w.norm(p=2, dim=[1, 2, 3]))
        elif isinstance(m, nn.Linear):
            w = m.weight

            lasso_ch.append(w.norm(p=2, dim=[0]))

            # not last fully connected layer
            if not hasattr(m, 'last'):
                lasso_ch.append(w.norm(p=2, dim=[1]))

    net.apply(group_lasso)

    return torch.cat(lasso_ch).sum()


class GroupLassoLoss(nn.modules.loss._Loss):
    """Add group lasso regularization to existing loss"""

    coef = None

    def __init__(self, loss_func, model, group_lasso_ratio=0.2):
        """
        Args:
            loss_func: original loss function
            model: Model on which to compute Group lasso penalty
            group_lasso_ratio: Group lasso regularization ratio = group_lasso / (group_lasso + loss)
        """
        super(GroupLassoLoss, self).__init__(reduction="none")
        self.loss_func = loss_func
        self.model = model
        self.group_lasso_ratio = group_lasso_ratio

    def forward(self, input, target):
        loss = self.loss_func(input, target)
        lasso_penalty = compute_group_lasso_penalty(self.model)
        #         print(f"loss={loss} lasso_penalty={lasso_penalty}")

        # auto-tune the group-lasso coefficient at first training iteration.
        # see PruneTrain paper, "Regularization Penalty Coefficient Setup" paragraph.
        if self.coef is None:
            with torch.no_grad():
                self.coef = self.group_lasso_ratio * loss / (lasso_penalty * (1 - self.group_lasso_ratio))
            print(
                f"set coef={self.coef} loss={loss} lasso_penalty={lasso_penalty} group_lasso_ratio={self.group_lasso_ratio}")

        loss += lasso_penalty * self.coef
        return loss


def count_channels(model):
    """Return model channel count"""
    count = 0
    for m in model.modules():
        if isinstance(m, nn.Conv2d):
            dims = list(m.weight.shape)
            count += dims[1] + dims[0]
        elif isinstance(m, nn.BatchNorm2d):
            dims = list(m.weight.shape)
            count += dims[0]
        elif isinstance(m, nn.Linear):
            dims = list(m.weight.shape)
            count += dims[1]
    return count


def multi_max(input, dims, keepdim=False):
    """
    Performs `torch.max` over multiple dimensions of `input`
    Adapted from https://discuss.pytorch.org/t/sum-mul-over-multiple-axes/1882/8
    """
    dims = sorted(dims)
    maxed = input
    for dim in reversed(dims):
        maxed, _ = maxed.max(dim, keepdim)
    return maxed


def dense_channels(m, threshold=1e-4):
    """Return dense channels for the given layer"""
    dense_in_chs, dense_out_chs = [], []

    with torch.no_grad():

        if type(m) is nn.Conv2d:
            w_abs = m.weight.abs()

            # force sparse input channels to zero
            max_input_chs = multi_max(w_abs, [0, 2, 3])
            for c, v in enumerate(max_input_chs):
                if v >= threshold:
                    dense_in_chs.append(c)

            # force sparse output channels to zero
            max_output_chs = multi_max(w_abs, [1, 2, 3])
            for c, v in enumerate(max_output_chs):
                if v >= threshold:
                    dense_out_chs.append(c)

            return {"dense_in_chs": dense_in_chs, "dense_out_chs": dense_out_chs}

        elif type(m) is nn.Linear:
            pass  # TODO linear


def model_dense_channels(model, threshold=1e-4):
    """Get only dense channels for each layer

    Returns:
        Dict of layer name to dict with in/out dense channels
    """
    layer_to_dense_chs = OrderedDict()
    for n, m in model.named_children():
        layer_to_dense_chs[n] = dense_channels(m, threshold=threshold)
    return layer_to_dense_chs


def model_conv_layers(model):
    """List convolution layers and its next layer, if any

    Returns:
        Tuple of dicts of layer name to its prev/next layer
    """
    conv_layers = [n for n, m in model.named_children() if isinstance(m, nn.Conv2d)]

    conv_layer_to_prev = {}  # layer name to previous layer name
    # iterate starting from second layer
    for i in range(1, len(conv_layers)):
        curr = conv_layers[i]
        conv_layer_to_prev[curr] = conv_layers[i - 1]

    # layer name to next layer name
    conv_layer_to_next = {v: k for k, v in conv_layer_to_prev.items()}

    return conv_layer_to_prev, conv_layer_to_next


def prune_channels_dim4(param, in_chs, out_chs):
    """Return dimension 4 Tensor with only specified in/out channels indices"""
    # TODO index by several lists (in & out channels) possible?
    with torch.no_grad():
        pruned = param.data[:, in_chs, :, :]
        pruned = pruned[out_chs, :, :, :].clone()
    return pruned


def update_param(param, data):
    """Set Parameter data to new value and reset gradient to force dimension refresh"""
    param.data = data
    param.grad = None  # will be re-created during next call to backward()


def prune_network(model, threshold=1e-4, opt=None):
    """Prune network using PruneTrain method.

    Code also inspired by https://github.com/Eric-mingjie/network-slimming/blob/master/vggprune.py#L137
    from "Learning Efficient Convolutional Networks Through Network Slimming (ICCV 2017)" PyTorch implementation.

    Args:
        model: Model to prune, will be modified in place
        threshold: channels with all weights below this threshold will be removed
        opt: Optimizer, e.g. SGD with Nesterov, to prune momentum buffer, state will be modified in place
    """

    # get only dense channels for each layer
    layer_to_dense_chs = model_dense_channels(model, threshold=threshold)

    # list convolution layers and its next layer, if any
    conv_layer_to_prev, conv_layer_to_next = model_conv_layers(model)

    # create new pruned model
    prev_conv_dense_out_chs_idx = None
    for n0, m0 in model.named_children():
        dense_chs = layer_to_dense_chs[n0]

        if isinstance(m0, nn.Conv2d):
            dense_in_chs_idx = dense_chs["dense_in_chs"]
            dense_out_chs_idx = dense_chs["dense_out_chs"]

            # input channels of this layer must match with output channels of previous layer
            if n0 in conv_layer_to_prev:
                prev_layer = conv_layer_to_prev[n0]
                # keep union of this input channels and previous output channels
                prev_dense_out_chs_idx = layer_to_dense_chs[prev_layer]["dense_out_chs"]
                dense_in_chs_idx = sorted(set(dense_in_chs_idx) | set(prev_dense_out_chs_idx))

            # out channels of this layer must match with input channels of next layer
            if n0 in conv_layer_to_next:
                next_layer = conv_layer_to_next[n0]
                # keep union of this out channels and next input channels
                next_dense_in_chs_idx = layer_to_dense_chs[next_layer]["dense_in_chs"]
                dense_out_chs_idx = sorted(set(dense_out_chs_idx) | set(next_dense_in_chs_idx))

            # channels pruned?
            if m0.in_channels != len(dense_in_chs_idx) or m0.out_channels != len(dense_out_chs_idx):
                # update Conv2D with new dense in/out channels count
                m0.in_channels = len(dense_in_chs_idx)
                m0.out_channels = len(dense_out_chs_idx)

                # copy dense channels only
                update_param(m0.weight, prune_channels_dim4(m0.weight, dense_in_chs_idx, dense_out_chs_idx))

                # prune channels in optimizer momentum buffers
                if opt is not None:
                    if m0.weight in opt.state:
                        mom0 = opt.state[m0.weight]['momentum_buffer']
                        mom0.data = prune_channels_dim4(mom0, dense_in_chs_idx, dense_out_chs_idx)
                    else:
                        print(f"{n0}.weight missing from opt state len={len(opt.state)}")

            # save output channels of last seen conv. layer
            prev_conv_dense_out_chs_idx = dense_out_chs_idx

        elif isinstance(m0, nn.BatchNorm2d):
            ch_idx = prev_conv_dense_out_chs_idx

            # channels pruned?
            if m0.num_features != len(ch_idx):
                # update new BatchNorm2d with dense in/out channels count
                m0.num_features = len(ch_idx)

                with torch.no_grad():
                    update_param(m0.weight, m0.weight.data[ch_idx].clone())
                    update_param(m0.bias, m0.bias.data[ch_idx].clone())

                    # update running stats (if track_running_stats=True)
                    m0.running_mean = m0.running_mean[ch_idx].clone()
                    m0.running_var = m0.running_var[ch_idx].clone()

                    # prune channels in optimizer momentum buffers
                    if opt is not None:
                        if m0.weight in opt.state:
                            mom0 = opt.state[m0.weight]['momentum_buffer']
                            mom0.data = mom0.data[ch_idx].clone()
                        else:
                            print(f"{n0}.weight missing from opt state len={len(opt.state)}")
                        if m0.bias in opt.state:
                            mom0 = opt.state[m0.bias]['momentum_buffer']
                            mom0.data = mom0.data[ch_idx].clone()
                        else:
                            print(f"{n0}.bias missing from opt state len={len(opt.state)}")

        elif isinstance(m0, nn.Linear):
            ch_idx = prev_conv_dense_out_chs_idx

            # channels pruned?
            if m0.in_features != len(ch_idx):
                m0.in_features = len(ch_idx)

                with torch.no_grad():
                    # only input modified, bias unchanged
                    update_param(m0.weight, m0.weight.data[:, ch_idx].clone())

                    # prune channels in optimizer momentum buffers
                    if opt is not None:
                        if m0.weight in opt.state:
                            mom0 = opt.state[m0.weight]['momentum_buffer']
                            mom0.data = mom0.data[:, ch_idx].clone()
                        else:
                            print(f"{n0}.weight missing from opt state len={len(opt.state)}")
        else:
            pass


def lowest_max_channel_weight(net):
    """Return lowest of input/output channel maximum weights"""

    def max_channel_weights(m):
        if type(m) is nn.Conv2d:
            w_abs = m.weight.abs()

            # input channels
            if not hasattr(m, 'first'):  # not first conv layer
                max_input_chs = multi_max(w_abs, [0, 2, 3])
            else:
                max_input_chs = None

            # output channels
            max_output_chs = multi_max(w_abs, [1, 2, 3])

            return max_input_chs, max_output_chs

        elif type(m) is nn.Linear:
            pass  # TODO linear

        return None, None

    with torch.no_grad():
        chs = []
        for n, m in net.named_children():
            max_in_chs, max_out_chs = max_channel_weights(m)
            if max_in_chs is not None:
                chs.append(max_in_chs)
            if max_out_chs is not None:
                chs.append(max_out_chs)

        return torch.cat(chs).min()

