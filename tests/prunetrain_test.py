from core import Timer
from prunetrain import *


class FlatConvNet(nn.Module):
    def __init__(self, num_classes=10, init_weights=True):
        super(FlatConvNet, self).__init__()

        self.conv1 = nn.Conv2d(1, 16, kernel_size=5, stride=1, padding=2)
        self.bn1 = nn.BatchNorm2d(16)
        self.relu1 = nn.ReLU()
        self.pool1 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv2 = nn.Conv2d(16, 32, kernel_size=5, stride=1, padding=2)
        self.bn2 = nn.BatchNorm2d(32)
        self.relu2 = nn.ReLU()
        self.pool2 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.fc = nn.Linear(7 * 7 * 32, num_classes)

        if init_weights:
            # deterministic parameter initialization
            for i, m in enumerate(self.modules()):
                if isinstance(m, nn.Conv2d):
                    m.weight.data.fill_(i)
                elif isinstance(m, nn.BatchNorm2d):
                    m.weight.data.fill_(i)
                    m.bias.data.zero_()
                elif isinstance(m, nn.Linear):
                    m.weight.data.fill_(i)
                    m.bias.data.zero_()

    def forward(self, x):
        out = x

        out = self.conv1(out)
        out = self.bn1(out)
        out = self.relu1(out)
        out = self.pool1(out)

        out = self.conv2(out)
        out = self.bn2(out)
        out = self.relu2(out)
        out = self.pool2(out)

        out = out.reshape(out.size(0), -1)
        out = self.fc(out)
        return out


cuda = torch.device('cuda')


def test_compute_group_lasso_penalty():
    net = FlatConvNet()
    mark_layers(net)

    lasso_penalty = compute_group_lasso_penalty(net)
    assert torch.allclose(lasso_penalty, lasso_penalty.new_tensor([50168.8046875]))


def test_compute_group_lasso_penalty_benchmark():
    net = FlatConvNet().to(cuda)
    mark_layers(net)

    num_runs = 10000

    print('Starting timer')
    timer = Timer(synch=torch.cuda.synchronize)

    print('compute_group_lasso_penalty()')
    for i in range(num_runs):
        compute_group_lasso_penalty(net)
    print(f'Finished in {timer():.4} seconds')


def test_dense_channels():
    net = FlatConvNet()

    # no channels pruned
    layer_to_dense_chs = model_dense_channels(net, threshold=0.1)
    chs = layer_to_dense_chs["conv1"]
    assert len(chs["dense_in_chs"]) == 1
    assert len(chs["dense_out_chs"]) == 16
    chs = layer_to_dense_chs["conv2"]
    assert len(chs["dense_in_chs"]) == 16
    assert len(chs["dense_out_chs"]) == 32

    # only conv layers
    assert layer_to_dense_chs["bn1"] is None

    # increase prune threshold
    layer_to_dense_chs = model_dense_channels(net, threshold=1.1)
    chs = layer_to_dense_chs["conv1"]
    assert len(chs["dense_in_chs"]) == 0
    assert len(chs["dense_out_chs"]) == 0
    chs = layer_to_dense_chs["conv2"]
    assert len(chs["dense_in_chs"]) == 16
    assert len(chs["dense_out_chs"]) == 32


def test_dense_channels_benchmark():
    net = FlatConvNet().to(cuda)

    num_runs = 1000

    print('Starting timer')
    timer = Timer(synch=torch.cuda.synchronize)

    print('model_dense_channels()')
    for i in range(num_runs):
        model_dense_channels(net)
    print(f'Finished in {timer():.4} seconds')


def test_lowest_max_channel_weight():
    net = FlatConvNet()
    min_w = lowest_max_channel_weight(net)
    assert min_w == 1
    assert isinstance(min_w, torch.Tensor)


def test_lowest_max_channel_weight_benchmark():
    net = FlatConvNet().to(cuda)

    num_runs = 1000

    print('Starting timer')
    timer = Timer(synch=torch.cuda.synchronize)

    print('lowest_max_channel_weight()')
    for i in range(num_runs):
        lowest_max_channel_weight(net)
    print(f'Finished in {timer():.4} seconds')


