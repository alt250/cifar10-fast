from time import time

from prunetrain import count_channels, lowest_max_channel_weight, prune_network

import fastai
from fastai.vision import *
from fastai.callbacks.mem import PeakMemMetric


class SparsityChannelsMetric:
    """Compute ratio of removed sparse channels metric"""

    def __init__(self, model):
        self.model = model
        self.channel_count_orig = count_channels(model)

    def sparsity_ch(self, input: Tensor, targs: Tensor) -> Rank0Tensor:
        channel_count = count_channels(self.model)
        sparsity = 1 - (channel_count / self.channel_count_orig)
        return input.new_tensor([sparsity])


class LowestMaxChannelWeightMetric:
    """Lowest of input/output channel maximum weights"""

    def __init__(self, model):
        self.model = model

    def min_ch_w(self, input: Tensor, targs: Tensor) -> Rank0Tensor:
        return lowest_max_channel_weight(self.model)


class ModelPruner(LearnerCallback):
    "Prune model periodically during training."
    def __init__(self, learn:Learner, prune_freq=10, threshold:float=1e-4):
        super().__init__(learn)
#         print(f"ModelPruner: learn.loss_func={learn.loss_func} learn.opt={learn.opt}")
        self.prune_freq = prune_freq
        self.threshold = threshold

    def on_epoch_end(self, epoch, n_epochs, **kwargs:Any)->None:
        epoch1 = epoch + 1  # convert from 0-based to 1-based
        if epoch1 % self.prune_freq == 0 and epoch1 < n_epochs:
            print(f"Pruning network at end of epoch {epoch}/{n_epochs}")
            prune_network(self.learn.model, threshold=self.threshold, opt=self.learn.opt.opt)


class TrainTimeMetric(Callback):
    "Report precise epoch train time"
    _order = -999  # run before other metrics

    name = "epoch_time"

    def __init__(self):
        self.epoch_times = []

    def on_train_begin(self, **kwargs: Any) -> None:
        self.train_start = time()

    def on_epoch_begin(self, **kwargs: Any) -> None:
        self.epoch_start = time()

    def on_epoch_end(self, last_metrics, **kwargs: Any) -> None:
        epoch_time = time() - self.epoch_start
        self.epoch_times.append(epoch_time)
        return add_metrics(last_metrics, epoch_time)

    def on_train_end(self, **kwargs: Any) -> None:
        self.train_total_time = time() - self.train_start


class PeakMemMetric(fastai.callbacks.mem.PeakMemMetric):
    """Rename metrics to make their name unique"""
    def on_train_begin(self, **kwargs):
        self.learn.recorder.add_metric_names(['cpu used', 'cpu peak', 'gpu used', 'gpu peak'])



