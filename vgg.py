# inspired by Pytorch VGG impl.: https://github.com/pytorch/vision/blob/master/torchvision/models/vgg.py

from torch_backend import *


def vgg_block(c_in, c_out, batch_norm=True, **kw):
    """VGG block with convolution + batch norm + ReLU"""
    block = {
        'conv1': nn.Conv2d(c_in, c_out, kernel_size=3, stride=1, padding=1, bias=False),
        'bn1': nn.BatchNorm2d(c_out, **kw),
        'relu1': nn.ReLU(True),
    }
    return block


# cfgs = {
#     'A': [64, 'M', 128, 'M', 256, 256, 'M', 512, 512, 'M', 512, 512, 'M'],
#     'B': [64, 64, 'M', 128, 128, 'M', 256, 256, 'M', 512, 512, 'M', 512, 512, 'M'],
#     'D': [64, 64, 'M', 128, 128, 'M', 256, 256, 256, 'M', 512, 512, 512, 'M', 512, 512, 512, 'M'],
#     'E': [64, 64, 'M', 128, 128, 'M', 256, 256, 256, 256, 'M', 512, 512, 512, 512, 'M', 512, 512, 512, 512, 'M'],
# }

class FlattenDim(nn.Module):
    """
    Flattens a contiguous range of dims in a tensor.
    https://pytorch.org/docs/stable/torch.html#torch.flatten
    """
    def __init__(self, start_dim):
        super().__init__()
        self.start_dim = start_dim

    def forward(self, x):
        return torch.flatten(x, self.start_dim)


# small VGG8 architecture for fast training and experiments
# adapted from PruneTrain src/models/cifar/vgg8_bn_flat.py
def VGG8(c=64, block=vgg_block, num_classes=10, img_size=32, **kw):
    """Create VGG8 network
    Args:
        c: Number of output channels of first conv. layer
        num_classes: Number of clases
        img_size: Size of image side in pixels, must be square and a multiple of 32
    Returns:
        Network description
    """

    if isinstance(c, int):
        c = [c, 2 * c, 4 * c, 8 * c, 8 * c]

    # adjust Linear layer input to different image dimensions
    img_size_ratio = int(img_size / 32)

    return {
        'stage1': {
            'block0': block(3, c[0], **kw),
            'maxpool0': nn.MaxPool2d(kernel_size=2, stride=2),
        },
        'stage2': {
            'block0': block(c[0], c[1], **kw),
            'maxpool0': nn.MaxPool2d(kernel_size=2, stride=2),
        },
        'stage3': {
            'block0': block(c[1], c[2], **kw),
            'maxpool0': nn.MaxPool2d(kernel_size=2, stride=2),
        },
        'stage4': {
            'block0': block(c[2], c[3], **kw),
            'maxpool0': nn.MaxPool2d(kernel_size=2, stride=2),
        },
        'stage5': {
            'block0': block(c[3], c[4], **kw),
            'maxpool0': nn.MaxPool2d(kernel_size=2, stride=2),
        },
        'final': {
            # reduce spacial resolution
            'resize': Identity() if img_size_ratio == 1 else nn.MaxPool2d(kernel_size=img_size_ratio,
                                                                          stride=img_size_ratio),
            'flatten': FlattenDim(start_dim=1),
            "fc": nn.Linear(c[4], num_classes, bias=True),
        },
        'classifier': Identity(),
    }


def VGG16(c=64, block=vgg_block, num_classes=10, img_size=32, **kw):
    """Create VGG16 (configuration E) network with a single Linear classifier
    Args:
        c: Number of output channels of first conv. layer
        num_classes: Number of clases
        img_size: Size of image side in pixels, must be square and a multiple of 32
    Returns:
        Network description
    """

    if isinstance(c, int):
        c = [c, 2 * c, 4 * c, 8 * c, 8 * c]

    # adjust Linear layer input to different image dimensions
    img_size_ratio = int(img_size / 32)

    return {
        'prep': {'conv': nn.Conv2d(3, c[0], kernel_size=3, stride=1, padding=1, bias=False)},
        'stage1': {
            'block0': block(c[0], c[0], **kw),
            'maxpool0': nn.MaxPool2d(kernel_size=2, stride=2),
        },
        'stage2': {
            'block0': block(c[0], c[1], **kw),
            'block1': block(c[1], c[1], **kw),
            'maxpool0': nn.MaxPool2d(kernel_size=2, stride=2),
        },
        'stage3': {
            'block0': block(c[1], c[2], **kw),
            'block1': block(c[2], c[2], **kw),
            'block2': block(c[2], c[2], **kw),
            'block3': block(c[2], c[2], **kw),
            'maxpool0': nn.MaxPool2d(kernel_size=2, stride=2),
        },
        'stage4': {
            'block0': block(c[2], c[3], **kw),
            'block1': block(c[3], c[3], **kw),
            'block2': block(c[3], c[3], **kw),
            'block3': block(c[3], c[3], **kw),
            'maxpool0': nn.MaxPool2d(kernel_size=2, stride=2),
        },
        'stage5': {
            'block0': block(c[3], c[4], **kw),
            'block1': block(c[4], c[4], **kw),
            'block2': block(c[4], c[4], **kw),
            'block3': block(c[4], c[4], **kw),
            'maxpool0': nn.MaxPool2d(kernel_size=2, stride=2),
        },
        'final': {
            # reduce spacial resolution
            'resize': Identity() if img_size_ratio == 1 else nn.MaxPool2d(kernel_size=img_size_ratio,
                                                                          stride=img_size_ratio),
            'flatten': FlattenDim(start_dim=1),
            "fc": nn.Linear(c[4], num_classes, bias=True),
        },
        'classifier': Identity(),
    }

