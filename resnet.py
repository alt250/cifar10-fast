from core import *
from torch_backend import *


def res_block(c_in, c_out, stride, **kw):
    block = {
        'bn1': batch_norm(c_in, **kw),
        'relu1': nn.ReLU(True),
        'branch': {
            'conv1': nn.Conv2d(c_in, c_out, kernel_size=3, stride=stride, padding=1, bias=False),
            'bn2': batch_norm(c_out, **kw),
            'relu2': nn.ReLU(True),
            'conv2': nn.Conv2d(c_out, c_out, kernel_size=3, stride=1, padding=1, bias=False),
        }
    }
    projection = (stride != 1) or (c_in != c_out)
    if projection:
        block['conv3'] = (nn.Conv2d(c_in, c_out, kernel_size=1, stride=stride, padding=0, bias=False), [rel_path('relu1')])
    block['add'] = (Add(), [(rel_path('conv3') if projection else rel_path('relu1')), rel_path('branch', 'conv2')])
    return block


def ResNet32(c=16, block=res_block, prep_bn_relu=False, concat_pool=True, **kw):
    """Create ResNet32 network
    Args:
        c: Number of output channels of first conv. layer
    Returns:
        Network description
    """

    if isinstance(c, int):
        c = [c, 2 * c, 4 * c, 4 * c]

    classifier_pool = {
        'in': Identity(),
        'maxpool': nn.MaxPool2d(kernel_size=8),
        'avgpool': (nn.AvgPool2d(kernel_size=8), [rel_path('in')]),
        'concat': (Concat(), [rel_path('maxpool'), rel_path('avgpool')]),
    } if concat_pool else {'pool': nn.MaxPool2d(kernel_size=8)}

    return {
        'prep': union({'conv': nn.Conv2d(3, c[0], kernel_size=3, stride=1, padding=1, bias=False)},
                      {'bn': batch_norm(c[0], **kw), 'relu': nn.ReLU(True)} if prep_bn_relu else {}),
        # stage 1
        'layer1': {
            'block0': block(c[0], c[0], 1, **kw),
            'block1': block(c[0], c[0], 1, **kw),
            'block2': block(c[0], c[0], 1, **kw),
            'block3': block(c[0], c[0], 1, **kw),
            'block4': block(c[0], c[0], 1, **kw),
        },
        # stage 2
        'layer2': {
            'block0': block(c[0], c[1], 2, **kw), # dimension change
            'block1': block(c[1], c[1], 1, **kw),
            'block2': block(c[1], c[1], 1, **kw),
            'block3': block(c[1], c[1], 1, **kw),
            'block4': block(c[1], c[1], 1, **kw),
        },
        # stage 3
        'layer3': {
            'block0': block(c[1], c[2], 2, **kw), # dimension change
            'block1': block(c[2], c[2], 1, **kw),
            'block2': block(c[2], c[2], 1, **kw),
            'block3': block(c[2], c[2], 1, **kw),
            'block4': block(c[2], c[2], 1, **kw),
        },
        'final': union(classifier_pool, {
            'flatten': Flatten(),
            'linear': nn.Linear(2 * c[2] if concat_pool else c[2], 10, bias=True),
        }),
        'classifier': Identity(),
    }
