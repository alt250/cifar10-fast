# Dynamic sparse network reconfiguration to reduce training time

This repository started as a fresh implementation of [PruneTrain](https://arxiv.org/abs/1901.09290) dynamic sparse 
network reconfiguration over David Page's [cifar10-fast](https://github.com/davidcpage/cifar10-fast) code to 
train CNN fast on a single GPU.

Later on, I ported the PruneTrain implementation to the [fastai](https://docs.fast.ai/) to benefit from the 
additional features provided by this library.

## Can sparse training reduce time-to-production of neural network models?

### Long model training time 

Training deep neural networks on large datasets can take a huge amount of time. This can last days or weeks or even be prohibitively long. 

When experimenting with new architectures or hyperparameters, long training times lengthen the duration before getting feedback on the model performance.

Once a predictive model has been deployed in production, it needs to be retrained periodically to avoid [model drift](https://en.wikipedia.org/wiki/Concept_drift) and update it based on data from a recent period, e.g. to better match new customers behavior. [Best practices](https://mlinproduction.com/model-retraining/) recommend to re-train the model using a long-running batch job, which increases the load on the infrastructure during all its duration.

One can use distributed processing on Cloud services with GPU/TPU acceleration to reduce training time. 
However, this has a cost proportional to the training time and adds complexity. 

### Model sparsity to speedup training

Most works on sparsity have focused on modifying pre-trained models to [create a sparse version](https://www.tensorflow.org/model_optimization/guide/pruning) ([paper](https://arxiv.org/pdf/1710.01878.pdf), 2017). The sparse model requires less memory and has reduced inference time. 
Sparsity can also be induced when training the model from scratch. 

Two kinds of sparsity can be created by pruning the model: 
* Non-structured sparsity: individual weights are zeroed out without a pattern. Those methods achieve the greatest accuracy compared to baseline dense model but provide no practical speedup during training due to lack of fine-grained hardware acceleration. One such method applied during initial training is [sparse learning](https://arxiv.org/pdf/1907.04840.pdf) (2019).
* Structured sparsity: parameters of whole blocks of weights, convolution kernels, channels or layers are pruned. This adds constraints to model which will typically achieve lower predictive performance than the dense baseline. However such methods can benefit from hardware acceleration. For instance, OpenAI released [block-sparse GPU accelerated operations](https://openai.com/blog/block-sparse-gpu-kernels/) (2017).

### Practical speedup?

To evaluate the practical speedup of CNN training with sparsity, I implement a structured pruning approach described in [PruneTrain: Fast Neural Network Training by Dynamic Sparse Model Reconfiguration](https://arxiv.org/abs/1901.09290) Sangkug Lym et al. 2019. The authors claim 1/3rd train-time reduction compared to several dense models without other optimization.

See the prunetrain notebook for results.
